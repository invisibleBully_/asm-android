package activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ss.bottomnavigation.BottomNavigation;

import fragment.AboutFragment;
import fragment.BudgetFragment;
import fragment.CustomerFragment;
import fragment.HQCommunicationFragment;
import fragment.MoMoEngagementFragment;
import fragment.MyProfileFragment;
import fragment.ProspectFragment;
import fragment.ReportFragment;
import fragment.SyncFragment;
import pegafrica.com.asm_mobile.R;
import util.CustomTypefaceSpan;
import util.FontCache;
import util.OnFragmentInteractionListener;


public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener {

    BottomNavigation bottomNavigation;
    DrawerLayout drawerLayout;
    Typeface typeface, typefaceBold;
    Toolbar toolbar;
    TextView headerEmail, toolbarHeader;
    String title = null;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerComponents();
    }


    @Override
    protected void onPause() {
        super.onPause();
        updateNavigationBarState();
    }


    @Override
    public void onResume() {
        super.onResume();
        updateNavigationBarState();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        Fragment fragment;
        if (id == R.id.nav_my_profile) {
            title = "MY PROFILE";
            fragment = new MyProfileFragment();
        } else if (id == R.id.nav_trainees) {
            title = "TRAINEES";
            fragment = new ProspectFragment();
        } else if (id == R.id.nav_budget) {
            title = "BUDGET";
            fragment = new BudgetFragment();
        } else if (id == R.id.nav_hq_communication) {
            title = "HQ Communication";
            fragment = new HQCommunicationFragment();
        } else if (id == R.id.nav_momo_engagement) {
            title = "MOMO ENGAGEMENT";
            fragment = new MoMoEngagementFragment();
        } else if (id == R.id.nav_hourly_update) {
            title = "HOURLY SALES UPDATE";
            fragment = new ReportFragment();
        } else if (id == R.id.nav_about) {
            title = " ABOUT";
            fragment = new AboutFragment();
        } else if (id == R.id.nav_logout) {
            confirmLogout();
            return false;
        } else {
            return false;
        }


        toolbarHeader.setText(title);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        clearStack();

        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void registerComponents() {
        typeface = FontCache.get("Lato-Regular.ttf", MainActivity.this);
        typefaceBold = FontCache.get("Lato-Bold.ttf", MainActivity.this);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        toolbarHeader = toolbar.findViewById(R.id.toolbar_text);
        toolbarHeader.setTypeface(typefaceBold);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }


        bottomNavigation = findViewById(R.id.bottom_navigation);
        View header = navigationView.getHeaderView(0);
        headerEmail = header.findViewById(R.id.nav_header_email_text_view);
        headerEmail.setTypeface(typeface);
        bottomNavigation.setTypeface(typeface);
        bottomNavigation.setDefaultItem(1);


        bottomNavigation.setOnSelectedItemChangeListener(itemId -> {


            FragmentTransaction transaction = null;
            switch (itemId) {
                case R.id.tab_share:
                    //transaction = getSupportFragmentManager().beginTransaction();
                    //Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                    //transaction.replace(R.id.content, new AddFundsFragment());
                    break;
                case R.id.tab_prospects:
                    title = "HPM";
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, new ProspectFragment());
                    break;
                case R.id.tab_scouting:
                    title = "SCOUTING";
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, new CustomerFragment());
                    break;
                case R.id.tab_sync:
                    title = "SYNC DATA";
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, new SyncFragment());
                    break;
                case R.id.tab_dsr:
                    title = "DSR RECRUITMENT";
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content, new ProspectFragment());
                    break;
                default:
                    //transaction = getSupportFragmentManager().beginTransaction();
                    //transaction.replace(R.id.content, new MyProfileFragment());
                    break;
            }


            toolbarHeader.setText(title);

            if (transaction != null) {
                clearStack();
                transaction.commit();
            }
        });

    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "Lato-Bold.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    private void updateNavigationBarState() {
        bottomNavigation.setSelectedItem(1);
    }


    public void clearStack() {
        int backStackEntry = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        getSupportFragmentManager().getFragments();
        if (getSupportFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
                Fragment mFragment = getSupportFragmentManager().getFragments().get(i);
                if (mFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
                }
            }
        }
    }


    private void confirmLogout() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.custom_logout);

        TextView confirmationText = dialog.findViewById(R.id.confirm_text);
        Button yes = dialog.findViewById(R.id.yes);
        Button no = dialog.findViewById(R.id.no);

        confirmationText.setTypeface(typefaceBold);
        yes.setTypeface(typefaceBold);
        no.setTypeface(typefaceBold);


        yes.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();

        });
        no.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }


}
